### pyhello project

#### what to know before mount the pipeline

* Understand or purpose the management flow, version and release (I use git flow)
* Understand how to install or use the future generated software (Is it a binary? A service?)
* Which tests do I need to apply to my pipeline?
* Understand if the project demands an external approvement to be deployed..

#### what are the stages of our pipeline

* quality
* build (binary or bundle)
* release (publish in pypi, npm, docker hub)
* deploy application (to server if needed)
* validation (tests with application running)
* notifications

#### on gitlab

* know which runners executors we have at our disposal for our project ([listed here](https://docs.gitlab.com/runner/executors/))
we cane list: shel, docker, docker+machine...
* check which variables are available ([listed here](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html))

