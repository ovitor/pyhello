from loguru import logger
from time import sleep

if __name__ == "__main__":
    logger.debug('Starting Python application.')
    print('Hello, from Python.')
    sleep(5)
    logger.info('Finishing Python application.')
